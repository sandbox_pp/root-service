FROM openjdk:11
EXPOSE 9090
COPY ./build/libs/root-service-0.0.1-SNAPSHOT.jar /usr/app/
WORKDIR /usr/app
ENTRYPOINT ["java","-jar","root-service-0.0.1-SNAPSHOT.jar"]
