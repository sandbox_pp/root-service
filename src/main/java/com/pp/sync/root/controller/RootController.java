package com.pp.sync.root.controller;

import com.pp.sync.root.service.RootService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RootController {

    @Autowired
    private RootService rootService;

    @GetMapping("/work/do/")
    public String doWork() {
        return rootService.doWork();
    }
}
