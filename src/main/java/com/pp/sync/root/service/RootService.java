package com.pp.sync.root.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class RootService {

    RestTemplate restTemplate = new RestTemplate();

    @Value("${service.a.url}")
    private String urlA;

    public String doWork() {
        var result = restTemplate.getForEntity(urlA, String.class);
        return "Root -> " + result.getBody();
    }
}
